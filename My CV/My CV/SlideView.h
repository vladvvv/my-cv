//
//  SlideView.h
//  My CV
//
//  Created by Vladislav on 04.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideView : UIView

+ (id) loadViewForPageAtIndex:(int)index;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
