//
//  ViewController.m
//  My CV
//
//  Created by Vladislav on 04.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import "ViewController.h"
#import "SlideView.h"

#define PAGES_COUNT 12

@interface ViewController ()<UIScrollViewDelegate, UIWebViewDelegate>{
    BOOL pageControlBeingUsed;
    int currentPage;
    NSMutableArray *pagesArray;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *currentPageIndicator;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageControlBeingUsed = NO;
    currentPage = 1;
    
    pagesArray = [NSMutableArray new];
    for (int i = 0; i < PAGES_COUNT; i++) {
        SlideView *slideView = [SlideView loadViewForPageAtIndex:i];
        [pagesArray addObject:slideView];
    }
    
    [self.currentPageIndicator setText:[NSString stringWithFormat:@"%d %@ %d", currentPage, NSLocalizedString(@"of", [NSString string]), PAGES_COUNT]];
    
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width * pagesArray.count, self.scrollView.frame.size.height);
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    for (int i = 0; i < PAGES_COUNT; i++) {
        CGRect frame;
        frame.origin.x = self.view.bounds.size.width * i;
        frame.origin.y = 0;
        frame.size.width = self.scrollView.frame.size.width;
        frame.size.height = self.scrollView.frame.size.height;
        
        SlideView *slideView = [pagesArray objectAtIndex:i];
        slideView.webView.delegate = self;
        [self.scrollView addSubview:slideView];
        
        [slideView setFrame:frame];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark
#pragma mark UIScrollViewDelegate implementation
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        CGFloat pageWidth = self.scrollView.frame.size.width;
        currentPage = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth);
        
        if(currentPage <= 0) currentPage = 1;
        
        [self.currentPageIndicator setText:[NSString stringWithFormat:@"%d %@ %d", currentPage, NSLocalizedString(@"of", [NSString string]), PAGES_COUNT]];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

#pragma mark
#pragma mark custom selectors for buttons
- (IBAction)nextButtonPressed:(id)sender{
    if(currentPage == PAGES_COUNT){
        //[self performSegueWithIdentifier:(NSString *)segue_MainMenuSegue sender:self];
    }else{
        pageControlBeingUsed = YES;
        CGRect frame;
        frame.origin.x = self.scrollView.frame.size.width * currentPage;
        frame.origin.y = 0;
        frame.size = self.scrollView.frame.size;
        [self.scrollView scrollRectToVisible:frame animated:YES];
        
        currentPage++;
        [self.currentPageIndicator setText:[NSString stringWithFormat:@"%d %@ %d", currentPage, NSLocalizedString(@"of", [NSString string]), PAGES_COUNT]];
    }
}

- (IBAction)backButtonPressed:(id)sender{
    if(currentPage == 1){
        
    }else{
        pageControlBeingUsed = YES;
        CGRect frame;
        frame.origin.x = self.scrollView.frame.size.width * (currentPage-2);
        frame.origin.y = 0;
        frame.size = self.scrollView.frame.size;
        [self.scrollView scrollRectToVisible:frame animated:YES];
        
        currentPage--;
        [self.currentPageIndicator setText:[NSString stringWithFormat:@"%d %@ %d", currentPage, NSLocalizedString(@"of", [NSString string]), PAGES_COUNT]];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    return YES;
}

@end
