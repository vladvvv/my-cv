//
//  SlideView.m
//  My CV
//
//  Created by Vladislav on 04.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import "SlideView.h"

@interface SlideView ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation SlideView

+ (id) loadViewForPageAtIndex:(int)index{
    SlideView *slideView = [[[NSBundle mainBundle] loadNibNamed:@"SlideView" owner:self options:nil] objectAtIndex:0];
    
    NSString *messageText = [NSString string];
    NSString *titleText = [NSString string];
    
    if(slideView){
        switch (index) {
            case 0:{
                titleText = NSLocalizedString(@"Contacts", [NSString string]);
                messageText = NSLocalizedString(@"E-mail: vladvolobuev@gmail.com\nPhone: +38 (099)-750-47-22\nAddress: Kiev, Sechenova st., 6", [NSString string]);
            }break;
                
            case 1:{
                titleText = NSLocalizedString(@"Objective", [NSString string]);
                messageText = NSLocalizedString(@"I am expecting to start a middle iOS developer career.", [NSString string]);
            }break;
                
            case 2:{
                titleText = NSLocalizedString(@"Summary", [NSString string]);
                messageText = NSLocalizedString(@"Experience in Objective-C iOS applications developing for more than 2 years. Also I have programming experience in C/C++/PHP/SQL/JavaScript. And I have experience of working in team.", [NSString string]);
            }break;
                
            case 3:{
                titleText = NSLocalizedString(@"Skills", [NSString string]);
                messageText = NSLocalizedString(@"- Good knowledge of Objective-C\n- Knowledge of Foundation Framework, UIKit, CoreData, adaptive layouts.\n- Experience in developing multithreaded applications, GCD                                                framework.\n- Knowledge of iOS Human Interface Guidelines.", [NSString string]);
            }break;
                
            case 4:{
                titleText = NSLocalizedString(@"Skills", [NSString string]);
                messageText = NSLocalizedString(@"- Experience in implementation of HTTP and RESTful services.\n- Knowledge of OOD patterns and principles.\n- Knowledge of computer science.\n- Experience of JavaScript and PHP development.\n- Knowledge of C/C++.\n- Knowledge of SQL.", [NSString string]);
            }break;
                
            case 5:{
                titleText = NSLocalizedString(@"Work experience 1", [NSString string]);
                messageText = NSLocalizedString(@"March 2016 - till now\nStartup\nkey responsibilities: iOS App Development.\nTechnology: Core Data, autolayout.", [NSString string]);
            }break;
                
            case 6:{
                titleText = NSLocalizedString(@"Work experience 2", [NSString string]);
                messageText = NSLocalizedString(@"February 2016 - March 2016\nOOO “Status Group” site developer.\nKey responsibilities: site development\nTechnology: joomla, php, javascript, html, css, mysql.", [NSString string]);
            }break;
                
            case 7:{
                titleText = NSLocalizedString(@"Work experience 3", [NSString string]);
                messageText = NSLocalizedString(@"December 2015 – January 2016\nOOO “Status Group” iOS Developer.\nKey responsibilities: iOS App Development.\nProject: “V-Center orders” ios application.\nTechnology: asynchronous http requests(NSURLSession), Objective-C Blocks, GCD, geolocation, singleton pattern, autolayout.", [NSString string]);
            }break;
                
            case 8:{
                titleText = NSLocalizedString(@"Work experience 4", [NSString string]);
                messageText = NSLocalizedString(@"May 2014 – February 2016\n“Eleven” and “Business Connections” (startup)\nKey responsibilities: iOS App development.\nTechnology: Parse.com integration, Facebook integration, Push notifications, Objective-C Blocks, GCD, geolocation, autolayout, localization.", [NSString string]);
            }break;
                
            case 9:{
                titleText = NSLocalizedString(@"Education", [NSString string]);
                messageText = NSLocalizedString(@"Taras Shevchenko National University of Kyiv, faculty of information technology, computer science, 2014 – present", [NSString string]);
            }break;
                
            case 10:{
                titleText = NSLocalizedString(@"Portfolio", [NSString string]);
                messageText = NSLocalizedString(@"1.Business Connections\niTunes link: https://itunes.apple.com/ru/app/business-connections/id1046472689?mt=8\nConnections is new effective tool for finding and keeping business contacts and organizing your business event more interactively.", [NSString string]);
            }break;
                
            case 11:{
                titleText = NSLocalizedString(@"Portfolio", [NSString string]);
                messageText = NSLocalizedString(@"2.V-Center orders\niTunes link: https://itunes.apple.com/ru/app/v-center-orders/id1078558697?mt=8\nApplications for windows engineer for ordering.", [NSString string]);
            }break;
                
            default:
                break;
        }
    }
    
    [slideView.titleLabel setText:titleText];
    [slideView.webView setOpaque:NO];
    slideView.webView.scrollView.scrollEnabled = NO;
    slideView.webView.scrollView.bounces = NO;
    [slideView.webView loadHTMLString:[NSString stringWithFormat:@"<div style='text-align:justify; font-size:17px;font-family:HelveticaNeue;background-color:transparent;color:#C59A6D;'>%@",[messageText stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]] baseURL:nil];
    
    return slideView;
}



@end
