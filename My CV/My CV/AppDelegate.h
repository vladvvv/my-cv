//
//  AppDelegate.h
//  My CV
//
//  Created by Vladislav on 04.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

