//
//  main.m
//  My CV
//
//  Created by Vladislav on 04.05.16.
//  Copyright © 2016 Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
